//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2003-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

#include "NewSession_m.h"

/**
 * In this step we'll introduce random numbers. We change the delay from 1s
 * to a random value which can be set from the NED file or from omnetpp.ini.
 * In addition, we'll "lose" (delete) the packet with a small probability.
 */
class SessionGenerator : public cSimpleModule
{
  private:
    cMessage *newSessionEvent;

    int sessionCount = 100;

    int sessionId;
    int pageCount;



  public:
    SessionGenerator();
    virtual ~SessionGenerator();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    NewSessionMessage* buildSessionMessage();
};

Define_Module(SessionGenerator);

SessionGenerator::SessionGenerator()
{
}

SessionGenerator::~SessionGenerator()
{
}

void SessionGenerator::initialize()
{
    // on init generate a session message an send
    newSessionEvent = new cMessage("newSessionEvent");
    sessionId = 0;
    pageCount = 0;

    scheduleAt(simTime(), newSessionEvent);
}

void SessionGenerator::handleMessage(cMessage *msg)
{
    if (msg == newSessionEvent && sessionId < sessionCount) {
        //  EV << "Generating new sessionMessage" << endl;
        NewSessionMessage *nsm = buildSessionMessage();
        send(nsm, "out");

        // schedule next session
        simtime_t sessionInterarrivalTime = par("sessionInterarrivalTime");
        // EV << "sessionInterarrivalTime: " << sessionInterarrivalTime << " s" << endl;
        scheduleAt(simTime() + sessionInterarrivalTime, newSessionEvent);
        // EV << "Generated Session " << sessionId << endl;
        sessionId++;
    }
}

NewSessionMessage* SessionGenerator::buildSessionMessage() {
    NewSessionMessage *nsm = new NewSessionMessage();
    nsm->setSessionId(sessionId);
    return nsm;
}


