//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2003-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include <stdio.h>
#include <fstream>
#include <string.h>
#include <omnetpp.h>

#include "Packet_m.h"

using namespace omnetpp;


/**
 * In this step we'll introduce random numbers. We change the delay from 1s
 * to a random value which can be set from the NED file or from omnetpp.ini.
 * In addition, we'll "lose" (delete) the packet with a small probability.
 */
class WebConsumer : public cSimpleModule
{
  private:
    int currentSession;
    int currentPage;

    std::ostringstream pageLevelLogStream;

  public:
    WebConsumer();
    virtual ~WebConsumer();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void finish() override;
    void logPacket(PacketMessage* packet);
};

Define_Module(WebConsumer);

WebConsumer::WebConsumer()
{
}

WebConsumer::~WebConsumer()
{
}

void WebConsumer::initialize()
{
    currentSession = 0;
    currentPage = -1;
}

void WebConsumer::handleMessage(cMessage *msg)
{
    // EV << "Received Packet" << endl;
    PacketMessage *packet = check_and_cast<PacketMessage *>(msg);
    //EV << "Session: " << packet->getSessionId() << "\tPage: " << packet->getPageId() << "\tSize: " << packet->getSize() << endl;
    logPacket(packet);
}

void WebConsumer::logPacket(PacketMessage* packet) {

    simtime_t_cref packetArrivalTime = packet->getArrivalTime();
    int packetSessionId = packet->getSessionId();
    int packetPageId = packet->getPageId();
    int packetId = packet->getId();
    int packetTotalPagesInSession = packet->getTotalPagesInSession();
    int packetTotalPageSize = packet->getTotalPageSize();
    int packetPageSizeLeft = packet->getPageSizeLeft();
    int packetSize = packet->getSize();

    pageLevelLogStream << packetArrivalTime << "," << packetSessionId << "," << packetPageId << "," <<
            packetId << "," << packetTotalPagesInSession << "," << packetTotalPageSize << "," << packetPageSizeLeft << "," <<
            packetSize << endl;
}

void WebConsumer::finish() {
    std::string pageLevelLog = "PacketArrivalTime,SessionId,PageId,PacketId,PagesInSession,TotalPageSize,PageSizeLeft,PacketSize";
    pageLevelLog += "\n";
    pageLevelLog += pageLevelLogStream.str();

    std::ofstream logFile("pageLevelLog.csv");
    logFile << pageLevelLog;
    logFile.close();
}


