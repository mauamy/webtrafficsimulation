import csv
import sys


def get_page_sizes(logfile):
    pages = {}

    with open(logfile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                session_page_key = "{}#{}".format(row[1], row[2])
                if session_page_key not in pages:
                    pages[session_page_key] = row[5]
            line_count += 1

    page_sizes = []
    for key in pages:
        page_sizes.append(pages[key])

    return page_sizes


def get_pages_iat(logfile):
    pages = {}
    with open(logfile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                session_page_key = "{}#{}".format(row[1], row[2])
                if session_page_key not in pages:
                    pages[session_page_key] = row[0]
            line_count += 1

    pages_start_times = [float(pages[id]) for id in pages]
    pages_iat = []
    for i in range(1, len(pages_start_times)):
        pages_iat.append(pages_start_times[i] - pages_start_times[i-1])

    return pages_iat


def get_packet_iat(logfile):
    packets_start_times = []
    with open(logfile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                packets_start_times.append(float(row[0]))
            line_count += 1

    packets_iat = []
    for i in range(1, len(packets_start_times)):
        packets_iat.append(packets_start_times[i] - packets_start_times[i-1])

    return packets_iat


def get_sessions(logfile):
    sessions = {}
    with open(logfile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                sessionId = row[1]
                if sessionId not in sessions:
                    sessions[sessionId] = {"start_time": row[0], "pages": row[4]}

            line_count += 1

    return sessions


def write_sessions_data(logfile):
    # write session interarrival time and pagePerSession file
    sessions = get_sessions(logfile)
    inter_arrival_times = []
    pages_per_session = [sessions[id]["pages"] for id in sessions]

    start_times = [float(sessions[id]["start_time"]) for id in sessions]
    for i in range(1,len(start_times)):
        inter_arrival_times.append(start_times[i] - start_times[i-1])

    with open("pagesPerSession", "w") as f:
        for pps in pages_per_session:
            f.write("{}\n".format(pps))

    with open("sessionInterarrivalTimes", "w") as f:
        for iat in inter_arrival_times:
            f.write("{}\n".format(iat))


def write_pages_data(logfile):
    # write pages sizes file
    pages_sizes = get_page_sizes(logfile)
    pages_iat = get_pages_iat(logfile)

    with open("pageSizes", "w") as f:
        for ps in pages_sizes:
            f.write("{}\n".format(ps))

    with open("pagesIat", "w") as f:
        for piat in pages_iat:
            f.write("{}\n".format(piat))


def write_packet_data(logfile):
    packets_iat = get_packet_iat(logfile)
    with open("packetIat", "w") as f:
        for piat in packets_iat:
            f.write("{}\n".format(piat))


if __name__ == '__main__':
    if len(sys.argv > 0):
        logfile = sys.argv[1]
    else:
        print("Usage: refactorData.py logfile.csv")
        sys.exit(1)

    write_sessions_data(logfile)
    write_pages_data(logfile)
    write_packet_data(logfile)
