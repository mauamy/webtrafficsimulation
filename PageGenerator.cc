//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2003-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

#include "NewPage_m.h"
#include "NewSession_m.h"

/**
 * In this step we'll introduce random numbers. We change the delay from 1s
 * to a random value which can be set from the NED file or from omnetpp.ini.
 * In addition, we'll "lose" (delete) the packet with a small probability.
 */
class PageGenerator : public cSimpleModule
{
  private:
    cMessage* newPageEvent;
    std::vector<NewPageMessage*> pages;

  public:
    PageGenerator();
    virtual ~PageGenerator();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    NewPageMessage* buildPageMessage(int sid, int pid, int totalPagesInSession);
    cGamma getTimeBetweenPagesFunction();
};

Define_Module(PageGenerator);

PageGenerator::PageGenerator()
{
}

PageGenerator::~PageGenerator()
{
}

void PageGenerator::initialize()
{
    newPageEvent = nullptr;
}

void PageGenerator::handleMessage(cMessage *msg)
{
    if (msg == newPageEvent) {
        if(pages.size() > 0) {
            // create new page event and send to packet generator
            NewPageMessage *npm = pages.at(0);
            send(npm, "out");
            pages.erase(pages.begin());
        }

        if(pages.size()) {
            // schedule next page
            cGamma gammaFunction = getTimeBetweenPagesFunction();
            simtime_t timeToNextPage = gammaFunction.draw(); // page interarrival time
            // EV << "Time to next page: " << timeToNextPage << " s" << endl;
            scheduleAt(simTime() + timeToNextPage, newPageEvent);
        } else {
            newPageEvent = nullptr;
        }
    } else {
        // handle new session
        NewSessionMessage *newSessionEvent = check_and_cast<NewSessionMessage *>(msg);
        // get number of pages for this session
        int newSessionId = newSessionEvent->getSessionId();
        int totalPages = int(par("pagesPerSession").doubleValue());
        for(int i=0; i<totalPages; i++) {
            NewPageMessage* npm = buildPageMessage(newSessionId, i, totalPages);
            pages.push_back(npm);
        }
        // EV << "New Session " << newSessionId <<  " with " << totalPages << " pages";
        if (newPageEvent == nullptr) {
            newPageEvent = new cMessage("newPageEvent");
            scheduleAt(simTime(), newPageEvent);
        }
    }

}


NewPageMessage* PageGenerator::buildPageMessage(int sid, int pid, int totalPagesInSession) {
   NewPageMessage *newPageMessage = new NewPageMessage();
   newPageMessage->setSessionId(sid);
   newPageMessage->setPageId(pid);
   newPageMessage->setTotalPagesInSession(totalPagesInSession);
   double pageSize = par("pageSize").doubleValue();
   // EV << "pageSize (double) = " << pageSize << " int = " << int(pageSize) << endl;
   newPageMessage->setPageSize(int(pageSize));

   return newPageMessage;
}


cGamma PageGenerator::getTimeBetweenPagesFunction() {
    cRNG *rng = getRNG(0);
    cGamma gamma;

    double rand = uniform(0, 1);
    if (rand < 0.1496) {
       gamma = cGamma(rng, 0.0932, 640.3334);
    } else if (rand < 0.3712) {
       gamma = cGamma(rng, 0.0699, 357.3947);
    } else if (rand < 0.5249) {
        gamma = cGamma(rng, 0.1327, 128.3409);
    } else if (rand < 0.8857) {
        gamma = cGamma(rng, 0.0874, 53.9234);
    } else {
        gamma = cGamma(rng, 0.1978, 90.332);
    }
    return gamma;
}

