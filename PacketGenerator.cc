//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2003-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

#include "NewPage_m.h"
#include "NewSession_m.h"
#include "Packet_m.h"

/**
 * In this step we'll introduce random numbers. We change the delay from 1s
 * to a random value which can be set from the NED file or from omnetpp.ini.
 * In addition, we'll "lose" (delete) the packet with a small probability.
 */
class PacketGenerator : public cSimpleModule
{
  private:
    cMessage *newPacketEvent;

    std::vector<PacketMessage*> packets;

    int sessionId;
    int pageId;
    int pageSize;

  public:
    PacketGenerator();
    virtual ~PacketGenerator();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    PacketMessage* buildPacketMessage(int sid, int pid, int totalPagesInSession, int pageSizeLeft, int totalPageSize);
    int getRandomPacketSize();
};

Define_Module(PacketGenerator);

PacketGenerator::PacketGenerator()
{

}

PacketGenerator::~PacketGenerator()
{
}

void PacketGenerator::initialize()
{
    newPacketEvent = nullptr;
}

void PacketGenerator::handleMessage(cMessage *msg)
{
    if (msg == newPacketEvent) {
        // EV << "Received newPacketEvent";
        // generate newPacketEvent and send it to out
        if(packets.size() > 0) {
            PacketMessage* packetMsg = packets.at(0);
            send(packetMsg, "out");
            packets.erase(packets.begin());
        }

        // schedule next packet if left pageSize is bigger than 0
        if(packets.size() > 0) {
            double timeToNextPacket = par("packetInterarrivalTime").doubleValue();
            scheduleAt(simTime() + timeToNextPacket, newPacketEvent);
        } else {
            newPacketEvent = nullptr;
        }
    } else {
        NewPageMessage *newPageMsg = check_and_cast<NewPageMessage *>(msg);

        pageSize = newPageMsg->getPageSize();
        int totalPageSize = pageSize;
        // EV << "Received new page message" << " (pageSize: " << pageSize << ") \n";
        while(pageSize > 0) {
            PacketMessage* packet = buildPacketMessage(newPageMsg->getSessionId(), newPageMsg->getPageId(), newPageMsg->getTotalPagesInSession(), pageSize, totalPageSize);
            packets.push_back(packet);
        }

        // EV << "Numbers of packets: " << packets.size() << endl;

        if (newPacketEvent == nullptr) {
            newPacketEvent = new cMessage("newPacketEvent");
            scheduleAt(simTime(), newPacketEvent);
        }

    }
}

PacketMessage* PacketGenerator::buildPacketMessage(int sid, int pid, int totalPagesInSession, int pageSizeLeft, int totalPageSize) {
    PacketMessage *packet = new PacketMessage();
    packet->setSessionId(sid);
    packet->setPageId(pid);
    packet->setTotalPagesInSession(totalPagesInSession);
    packet->setPageSizeLeft(pageSizeLeft);
    packet->setTotalPageSize(totalPageSize);

    // if packetsize is bigger than left page size set it to left page size
    int packetSize = getRandomPacketSize();
    if (packetSize > pageSize) {
        packetSize = pageSize;
    }
    packet->setSize(packetSize);

    pageSize -= packetSize;

    return packet;
}

int PacketGenerator::getRandomPacketSize() {
    double rand = uniform(0, 1);
    if (rand < 0.3017) {
       return 40;
    } else if (rand < 0.3017 + 0.1227) {
       return 552;
    } else if (rand < 0.3017 + 0.1227 + 0.1308) {
        return 576;
    }
    return 1500;
}



